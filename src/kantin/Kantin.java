package kantin;

import javax.swing.JOptionPane;

public class Kantin {
    //deklarasi

    private String penjual;
    private String pembeli;
    private String jenisMakanan;
    private int harga;

    //setter
    public void setPenjual(String penjual) {
        this.penjual = penjual;
    }

    public void setPembeli(String pembeli) {
        this.pembeli = pembeli;
    }

    public void setJenisMakanan(String jenisMakanan) {
        this.jenisMakanan = jenisMakanan;
    }

    //getter
    public String getPenjual() {
        return penjual;
    }

    public String getPembeli() {
        return pembeli;
    }

    public String getJenisMakanan() {
        return jenisMakanan;
    }

    public int getHarga() {
        return harga;
    }

    public void setHarga(int harga) {
        this.harga = harga;
    }

    //method tambahan
    String menjualBarang;
    String ramai;

}

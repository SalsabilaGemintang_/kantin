
import javax.swing.JOptionPane;

public class kantinKantor {

    private String diKantor;
    private String siapSaji;

    public void setDiKantor(String diKantor) {
        this.diKantor = diKantor;
    }

    public void setSiapSaji(String siapSaji) {
        this.siapSaji = siapSaji;
    }

    public String getDiKantor() {
        return diKantor;
    }

    public String getSiapSaji() {
        return siapSaji;
    }

}

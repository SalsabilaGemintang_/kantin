
import javax.swing.JOptionPane;

public class kantinSekolah {

    private String diSekolah;
    private String lebihMurah;

    public void setDiSekolah(String diSekolah) {
        this.diSekolah = diSekolah;
    }

    public void setLebihMurah(String lebihMurah) {
        this.lebihMurah = lebihMurah;
    }

    public String getDiSekolah() {
        return diSekolah;
    }

    public String getLebihMurah() {
        return lebihMurah;
    }

}
